import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  private advertisements: Array<object> = [];

  constructor(private restService: RestService) { }

  ngOnInit() {
    this.getAdvertisements();
  }

  public getAdvertisements() {
    this.restService.getAdvertisements().subscribe((data: Array<object>) => {
    this.advertisements = data;
    console.log(data);
    });
  }

  Articles: object = [
    {
      id: 1,
      title: 'title 1',
      content: 'lorem ipsum 1'
    },
    {
      id: 2,
      title: 'title 2',
      content: 'lorem ipsum 2'
    },
    {
      id: 3,
      title: 'title 3',
      content: 'lorem ipsum 3'
    },
    {
      id: 4,
      title: 'title 4',
      content: 'lorem ipsum 4'
    },
  ];







}
